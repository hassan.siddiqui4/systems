//
//  Reachability.swift
//  SystemMovie
//
//  Created by Hassan on 18/05/2022.
//

import Foundation
import RxSwift
import Alamofire

func networkAvailable() -> Observable<Bool> {
    return NetworkManager.shared.reach
}

private class NetworkManager: NSObject {

    static let shared = NetworkManager()

    let reachSubject = ReplaySubject<Bool>.create(bufferSize: 1)
    var reach: Observable<Bool> {
        return reachSubject.asObservable()
    }

    override init() {
        super.init()

        NetworkReachabilityManager.default?.startListening(onUpdatePerforming: { (status) in
            switch status {
            case .notReachable:
                self.reachSubject.onNext(false)
            case .reachable:
                self.reachSubject.onNext(true)
            case .unknown:
                self.reachSubject.onNext(false)
            }
        })
    }
}
