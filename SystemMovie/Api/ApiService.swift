//
//  ApiClass.swift
//  SystemMovie
//
//  Created by Hassan on 18/05/2022.
//

import Foundation
import Alamofire

class ApiService {
    
    typealias DefaultAPIFailureClosure = (NSError) -> Void
    typealias DefaultAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void
    typealias DefaultBoolResultAPISuccesClosure = (Bool) -> Void
    typealias DefaultArrayResultAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void

    
    func  callApiRequest<T : Codable>(route: String,method:HTTPMethod, parameters: Parameters,
                                            success:@escaping (T) -> Void,
                                            responseType:T.Type,
                                            failure:@escaping DefaultAPIFailureClosure,
                                            errorPopup: Bool,
                                            showLoader: Bool){
        
        
        var routeURL = GETURLfor(route: route, parameters: parameters)!
        var encoding:ParameterEncoding!
        
        if (method == .get) {
           encoding = URLEncoding.default
        } else{
           routeURL = POSTURLforRoute(route: route)!
           encoding = JSONEncoding.default
        }
        
        
        AF.request(routeURL, method: method, parameters: parameters, encoding: encoding, headers: getAuthorizationHeader()).responseJSON { (response) in
            print(routeURL)
            print(response)

          //  PKHUD.sharedHUD.hide()

            switch response.result {
            case .success(let data):
                
                if let response =  Decoder.decodeJson(dictionary: data as! Dictionary<String, AnyObject>, responseType: responseType) {
                        success(response)
                }
                                
            case .failure(let error):
                    failure(error as NSError)
                break
                
            }
        }
    }
    
    
    func getAuthorizationHeader () -> HTTPHeaders {
        let headers: HTTPHeaders = ["Accept":"application/json","Content-Type" :"application/json"]
        return headers
    }
    
    
    func POSTURLforRoute(route:String) -> URL?{
        if let components: NSURLComponents = NSURLComponents(string: (ApiConstant.BASE_URL+route)){
            return components.url! as URL
        }
         return nil
    }

    func GETURLfor(route:String, parameters: Parameters) -> URL?{
        var queryParameters = ""
        for key in parameters.keys {
            if queryParameters.isEmpty {
                queryParameters =  "?\(key)=\((String(describing: (parameters[key]!))).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)"
            } else {
                queryParameters +=  "&\(key)=\((String(describing: (parameters[key]!))).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)"
            }
            queryParameters =  queryParameters.trimmingCharacters(in: .whitespaces)
            
        }
        if let components: NSURLComponents = NSURLComponents(string: (ApiConstant.BASE_URL+route+queryParameters)){
            return components.url! as URL
        }
        return nil
    }
    

}
