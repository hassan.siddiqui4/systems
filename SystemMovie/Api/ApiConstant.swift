//
//  ApiConstant.swift
//  SystemMovie
//
//  Created by Hassan on 18/05/2022.
//

import Foundation


struct ApiConstant {
    static let BASE_URL = "http://api.themoviedb.org/3/"
    static let GET_CITY_LIST = "cities"
    static let IMAGE_BASE = "https://image.tmdb.org/t/p/original"
    static let APIKEY = "328c283cd27bd1877d9080ccb1604c91"
    
    static let MOVIE_LIST = "discover/movie"

}
