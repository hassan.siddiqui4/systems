//
//  Decoder.swift
//  SystemMovie
//
//  Created by Hassan on 18/05/2022.
//

import Foundation


class Decoder:NSObject {
    static func decodeJson<T : Decodable>(dictionary: Dictionary<String, AnyObject>, responseType:T.Type) -> T? {
        do {
            let data = try JSONSerialization.data(withJSONObject: dictionary)
            let gitData =  try JSONDecoder().decode(responseType, from: data)
            return gitData
        } catch {
            print(error)
        }
        
        return nil
    }
}
