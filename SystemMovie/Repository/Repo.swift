//
//  Repo.swift
//  SystemMovie
//
//  Created by Hassan on 18/05/2022.
//

import Foundation
import Alamofire
import RxSwift

class Repo {
        
    func getMovieList(sort:String,page:String,getResponse: @escaping ([Details]) -> Void){
       
        let params : Parameters = [
            "api_key": ApiConstant.APIKEY,
            "primary_release_date.lte":"2016-12-31",
            "sort_by":sort,
            "page": page,
        ]
        
        ApiService.init().callApiRequest(route: ApiConstant.MOVIE_LIST , method: .get, parameters: params, success: { (response) in
            getResponse(response.results ?? [])
             
        }, responseType: ListModel.self, failure: { (error) in
            
        }, errorPopup: true, showLoader: true)
        
    }
    
    
    func getMovieDetails(movieId: String,getResponse: @escaping (MovieDetailsModel) -> Void){
       
        let params : Parameters = [
            "api_key": ApiConstant.APIKEY,
        ]
           
        let url = "movie/\(movieId)"

        ApiService.init().callApiRequest(route: url , method: .get, parameters: params, success: { (response) in
            getResponse(response)
             
        }, responseType: MovieDetailsModel.self, failure: { (error) in
            
        }, errorPopup: true, showLoader: true)
        
    }
    
    
}
