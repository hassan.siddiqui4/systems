//
//  BaseVC.swift
//  SystemMovie
//
//  Created by Hassan on 18/05/2022.
//

import Foundation
import UIKit

class BaseVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupListners()
    }
    
    func setupViews(){
        
    }
    
    func setupListners(){
        
    }
    
}

enum Order {
  case Default, Alphabetical, Rating
    
    func value() -> String{
            switch self {
            case .Default:
                return "release_date.desc"
            case .Alphabetical:
                return "alphabetical"
            case .Rating:
                return "popularity.desc"
            }
        }
}
