//
//  MovieDetailsVC.swift
//  SystemMovie
//
//  Created by Hassan on 18/05/2022.
//

import UIKit
import RxSwift

class MovieDetailsVC: BaseVC {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var synopsis: UILabel!
    @IBOutlet weak var movieDetails: UITextView!
    @IBOutlet weak var duration: UILabel!
    var movieDetailsVM : MovieDetailsVM?
    var movieId : String?
    let disposeBag = DisposeBag()

    
    class func create(movieId : String,movieDetailsVM: MovieDetailsVM?) -> MovieDetailsVC {
        let board = UIStoryboard(name: "Main", bundle: nil)
        let vc = board.instantiateViewController(withIdentifier: "MovieDetailsVC") as! MovieDetailsVC
        vc.movieId = movieId
        vc.movieDetailsVM = movieDetailsVM
        return vc
    }
        
    override func setupViews() {
        
        movieDetailsVM?.getMovie(movieId: movieId ?? "")
        
        
        movieDetailsVM?.subject.subscribe(onNext:{ movieDetails in
           
            self.movieTitle.text = movieDetails.originalTitle
            self.movieDetails.text = movieDetails.overview
            self.image.setImageWithLoader(urlString: ApiConstant.IMAGE_BASE.appending(movieDetails.posterPath ?? ""))
            self.movieTitle.text = movieDetails.originalTitle
            self.language.text = movieDetails.originalLanguage
            
            if let genres = movieDetails.genres{
                var completeGenre : String = ""
                for genre in genres {
                    completeGenre.append(genre.name ?? "")
                    completeGenre.append(",")
                }
                self.synopsis.text = completeGenre
                if self.synopsis.text?.count ?? 0 > 0{
                    self.synopsis.text?.removeLast()
                }
            }
            
            
            if let runtime = movieDetails.runtime{
               self.duration.text = String(runtime)
            }
            self.title = movieDetails.originalTitle

        }).disposed(by: disposeBag)
        
    }
    

}
