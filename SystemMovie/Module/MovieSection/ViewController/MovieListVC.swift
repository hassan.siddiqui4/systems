//
//  ViewController.swift
//  SystemMovie
//
//  Created by Hassan on 18/05/2022.
//

import UIKit
import RxSwift

class MovieListVC: BaseVC {

    
    @IBOutlet weak var movieListTV: UITableView!
    let refreshControl = UIRefreshControl()
    var pageCount:Int = 1
    var sortOrder:String = Order.Rating.value()

    let homeViewMode = HomeMovieVM(repo: Repo())
    let disposeBag = DisposeBag()
    
    override func setupViews() {
        
        self.title = "Movie List"
        let rightBarButtonItem = UIBarButtonItem.init(title: "Filter", style: .done, target: self, action:  #selector(filterMovie))

        self.navigationItem.rightBarButtonItem = rightBarButtonItem

        // Getting Data From ViewModel
        homeViewMode.getMovie(sortOrder: sortOrder, page: String(pageCount))
        
        // Setting up TableVIew Delegate
        movieListTV.rx.setDelegate(self).disposed(by: disposeBag)
        
        // Binding TableView To Publisher
        homeViewMode.subject.bind(to: movieListTV.rx.items(cellIdentifier: String(describing: MovieTVC.self), cellType: MovieTVC.self)) { (row,item,cell) in
                  cell.desc.text = item.overview
                  cell.name.text = item.title
                  if let popularity =  item.popularity{
                        cell.rating.text = String(popularity)
                  }
                  cell.movieImage.setImageWithLoader(urlString: ApiConstant.IMAGE_BASE.appending(item.posterPath ?? ""))
           
              }.disposed(by: disposeBag)
        
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        movieListTV.addSubview(refreshControl)
    }
    
    override func setupListners() {
        // Getting Model and Navigating to Movie Details Screen
        movieListTV
                .rx
                .modelSelected(Details.self)
                .subscribe(onNext: { (model) in
                    // Naviage to MovieDetailsVC and Injecting Repository
                    let vc = MovieDetailsVC.create(movieId: String(model.id!), movieDetailsVM: MovieDetailsVM(repo: Repo()))
                    self.navigationController?.pushViewController(vc, animated: true)
                }).disposed(by: disposeBag)
        
    }
    
    @objc func didPullToRefresh() {
        // updating page count to 1 and initiating API Call
        pageCount -= 1
        if pageCount == 0 {
            pageCount = 1
        }
        homeViewMode.getMovie(sortOrder: sortOrder,page: String(pageCount))
        refreshControl.endRefreshing()
    }
    
    @objc func filterMovie(sender: UIBarButtonItem) {
        self.showSimpleActionSheet()
    }
    
    
    func showSimpleActionSheet() {
            let alert = UIAlertController(title: "Sort Order", message: "Select Sort Order", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Release date", style: .default, handler: { (_) in
                self.sortOrder = Order.Default.value()
                self.pageCount = 1
                self.homeViewMode.getMovie(sortOrder: self.sortOrder,page: String(self.pageCount))
            }))

            alert.addAction(UIAlertAction(title: "Alphabetical", style: .default, handler: { (_) in
                self.sortOrder = Order.Alphabetical.value()
                self.pageCount = 1
                self.homeViewMode.getMovie(sortOrder: self.sortOrder,page: String(self.pageCount))

            }))

            alert.addAction(UIAlertAction(title: "Rating", style: .default, handler: { (_) in
                self.sortOrder = Order.Rating.value()
                self.pageCount = 1
                self.homeViewMode.getMovie(sortOrder: self.sortOrder,page: String(self.pageCount))
            }))


            self.present(alert, animated: true, completion: {
            })
        }


}


extension MovieListVC: UITableViewDelegate {
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset - currentOffset <= 10.0 {
            pageCount += 1
            self.homeViewMode.getMovie(sortOrder: sortOrder,page: String(pageCount))
        }
    }
    
}

