//
//  MovieDetailsVM.swift
//  SystemMovie
//
//  Created by Hassan on 18/05/2022.
//

import UIKit
import RxSwift

class MovieDetailsVM: NSObject {
    
    var repo : Repo?
    let subject = PublishSubject<MovieDetailsModel>()

    
    init(repo:Repo) {
        self.repo = repo
    }
    
    func getMovie(movieId:String){
        
        repo?.getMovieDetails(movieId: movieId, getResponse: { response in
            let listModel : MovieDetailsModel = response
            self.subject.onNext(listModel)
            //self.subject.onCompleted()
        })
        
    }
    
}
