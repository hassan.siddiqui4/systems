//
//  HomeMovieVM.swift
//  SystemMovie
//
//  Created by Hassan on 18/05/2022.
//

import Foundation
import RxSwift

class HomeMovieVM {
    
    var repo : Repo?
    let subject = PublishSubject<[Details]>()

    
    init(repo:Repo) {
        self.repo = repo
    }
    
    func getMovie(sortOrder: String,page:String){
        
        repo?.getMovieList(sort: sortOrder, page: page) { response in
            let listModel : [Details] = response
            self.subject.onNext(listModel)
            //self.subject.onCompleted()
        }
    }
    
}
