//
//  KF+ImageView.swift
//  SystemMovie
//
//  Created by Hassan on 18/05/2022.
//

import UIKit
import RxCocoa
import RxSwift
import Kingfisher

//extension Reactive where Base: UIImageView {
//
//    public var imageURL: Binder<URL?> {
//        return self.imageURL(withPlaceholder: nil)
//    }
//
//    public func imageURL(withPlaceholder placeholderImage: UIImage?, options: KingfisherOptionsInfo? = []) -> Binder<URL?> {
//        return Binder(self.base, binding: { (imageView, url) in
//            imageView.kf.setImage(with: url,
//                                  placeholder: placeholderImage,
//                                  options: options,
//                                  progressBlock: nil,
//                                  completionHandler: { (result) in })
//        })
//    }
//}

extension UIImageView{
    
    func setImageWithLoader(urlString:String){
        let url = URL(string: urlString)
        let placeholderImage:UIImage?
        placeholderImage = UIImage(named: "")
        self.kf.indicatorType = .activity
    
    self.kf.setImage(
        with: url,
        placeholder: placeholderImage,
        options: [.scaleFactor(UIScreen.main.scale),
            .transition(.fade(1)),
            .cacheOriginalImage
        ])
    }
}
