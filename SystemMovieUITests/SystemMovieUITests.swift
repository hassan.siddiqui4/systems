//
//  SystemMovieUITests.swift
//  SystemMovieUITests
//
//  Created by Hassan on 18/05/2022.
//

import XCTest


class FirstControllerTest: XCTestCase {
    
    func makeSUT() -> MovieListVC {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let sut = storyboard.instantiateViewController(identifier: "MovieListVC") as! MovieListVC
        sut.loadViewIfNeeded()
        
        XCTAssertEqual(sut.title, "TITLE")

        
        return sut
    }
}


class SecondControllerTest: XCTestCase {
    
    func makeSUT() -> MovieDetailsVC {

        let sut = MovieDetailsVC.create(movieId: String("2222"), movieDetailsVM: MovieDetailsVM(repo: Repo()))

                
        XCTAssertEqual(sut.language.text, "TITLE")
        XCTAssertEqual(sut.synopsis.text, "Failure")
        sut.loadViewIfNeeded()

        return sut
    }
   
}

class SystemMovieUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
    
    
    
    
}

