//
//  SystemMovieTests.swift
//  SystemMovieTests
//
//  Created by Hassan on 18/05/2022.
//

import XCTest

class SystemMovieTests: XCTestCase {

    var mockAPIService = MockRepo()
    var rep = Repo()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        setUp()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
                
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
            setUp()
        }
    }
    
    override func setUp() {
        super.setUp()
        let vm = HomeMovieVM(repo: rep)
        vm.getMovie(sortOrder: Order.Alphabetical.value(), page:"1")
        
        let vm2 = MovieDetailsVM(repo: rep)
        vm2.getMovie(movieId: "123213123")
        
        vm2.getMovie(movieId: "10020")
        
    }
    

    override func tearDown() {
        super.tearDown()
    }

}

class MockRepo {
//    func getMovieList(sort:String,page:String,getResponse: @escaping ([Details]) -> Void){
//        //let details : [Details]?
//        //getResponse(details!)
//    }
}
